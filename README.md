# Image Resize Service

## Repository Structure

```
.
├── README.md
├── img-resize
│   ├── backend
│   ├── broker
│   ├── client
│   ├── docker-compose.yml
│   ├── run-scaled.sh
│   ├── storage
│   └── worker
└── oas
    └── oas-image-resize.yml
```

- The "img-resize" folder contains the application code. It contains a subfolder
  for each system component.
- The "oas" folder contains the OpenAPI Specification used for the backend
  service. It was used to generate the backend server structure.

## API

| Verb   | Path                    | Description                    |
| ------ | ----------------------- | ------------------------------ |
| POST   | `/images`               | Upload a new image             |
| GET    | `/images/{name}`        | Download an image              |
| DELETE | `/images/{name}`        | Delete an image                |
| PUT    | `/images/{name}/resize` | Start a job to resize an image |
| GET    | `/jobs/{id}`            | Get a job status               |

For other information, such as status codes and schemas, please see the OpenAPI
Specification.

## Running the Application

How to run the application:

```bash
docker compose -f "img-resize/docker-compose.yml" up -d --build
# or
cd img-resize
./run-scaled.sh
```

When services are running, you can:

```bash
cd img-resize/client

./downloader.sh
./uploader.sh
./resizer.sh
```

To better notice the application results, you can visit:

- the MinIO console on http://localhost:9001/buckets/bkt-images/browse (login
  with miniouser:miniouser)
- the RabbitMQ management console: http://localhost:15672/ (login with
  guest:guest)

## Architecture

A stateless **backend** server, written in Go, receives the user's HTTP request.
Images are stored in an S3-compatible **object storage**, MinIO. For
long-running jobs, the backend just publishes a job to a **message broker**
(RabbitMQ) and immediately sends a 202 Accepted response to the client with some
information about the job. The user is supposed to poll the backend until the
job terminated. In the meanwhile, a set of **workers**, written in Python (but
incomplete), pull jobs from the queue and execute it. Once a worker has finished
its job, it should persist this information in a database or in a bucket of the
object storage,

## TODO

- implement DELETE /images/{name}
- complete worker (now there is a sleep instead of the real job)
- persist job information
- passing configuration data without hardcoding it
- support backend scaling
- write tests
- improve logging

### Note on Messages ACKs

At the moment, message acknowledgments are disabled. This means that if a
consumer dies before completing its job, that job is lost. Once RabbitMQ
delivers message to the consumer, it immediately marks it for deletion.

> In order to make sure a message is never lost, RabbitMQ supports message
> acknowledgments. An ack(nowledgement) is sent back by the consumer to tell
> RabbitMQ that a particular message had been received, processed and that
> RabbitMQ is free to delete it.
> 
> If a consumer dies (its channel is closed, connection is closed, or TCP
> connection is lost) without sending an ack, RabbitMQ will understand that a
> message wasn't processed fully and will re-queue it. If there are other
> consumers online at the same time, it will then quickly redeliver it to
> another consumer. That way you can be sure that no message is lost, even if
> the workers occasionally die.

The message queue is durable, so that jobs survive in case of RabbitMQ failures.