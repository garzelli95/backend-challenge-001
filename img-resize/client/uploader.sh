#!/bin/bash

curl -i -X POST "http://localhost:80/api/v1/images" \
    -H "Content-Type: image/jpeg" \
    --data-binary "@gecko.jpg"
