#!/bin/bash

fname=$(curl -X POST "http://localhost:80/api/v1/images" \
    -H "Content-Type: image/jpeg" \
    --data-binary "@gecko.jpg" \
    | jq -r '.name')

curl -s -X GET "http://localhost:80/api/v1/images/$fname" -o "downloaded.jpg"
