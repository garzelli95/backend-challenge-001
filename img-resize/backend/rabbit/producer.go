package rabbit

import amqp "github.com/rabbitmq/amqp091-go"

type Producer struct {
	Connection *amqp.Connection
	Channel    *amqp.Channel
	Queue      amqp.Queue
}

func (p *Producer) Close() {
	p.Connection.Close()
	p.Channel.Close()
}
