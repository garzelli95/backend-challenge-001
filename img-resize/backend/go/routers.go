/*
 * Image Resize
 *
 * Upload, Download, Delete and Resize images.
 *
 * API version: 1.0.1
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/minio/minio-go/v7"
	ra "img-resize.com/backend/rabbit"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

func NewRouter(s3Client *minio.Client, rmqProducer *ra.Producer) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	routes := buildRoutes(s3Client, rmqProducer)

	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	return router
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World!")
}

type Routes []Route

func buildRoutes(s3Client *minio.Client, rmqProducer *ra.Producer) Routes {
	return Routes{

		Route{
			"Index",
			"GET",
			"/api/v1/",
			Index,
		},

		Route{
			"DeleteImage",
			strings.ToUpper("Delete"),
			"/api/v1/images/{name}",
			DeleteImage,
		},

		Route{
			"DownloadImage",
			strings.ToUpper("Get"),
			"/api/v1/images/{name}",
			DownloadImage(s3Client),
		},

		Route{
			"ResizeImage",
			strings.ToUpper("Put"),
			"/api/v1/images/{name}/resize",
			ResizeImage(rmqProducer),
		},

		Route{
			"UploadImage",
			strings.ToUpper("Post"),
			"/api/v1/images",
			UploadImage(s3Client),
		},

		Route{
			"GetJobById",
			strings.ToUpper("Get"),
			"/api/v1/jobs/{id}",
			GetJobById,
		},
	}
}
